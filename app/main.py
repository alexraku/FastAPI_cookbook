from fastapi import FastAPI, Path
from contextlib import asynccontextmanager
from sqlalchemy import select, desc

from app.cookbook import models
from app.cookbook import schemas
from app.cookbook.database import engine, session


@asynccontextmanager
async def lifespan(app: FastAPI):
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)
    yield
    await session.close()
    await engine.dispose()


app = FastAPI(title="Cook book", lifespan=lifespan)


@app.get("/")
async def it_works():
    """
    To check the operability.
    """
    return {"message": "it works"}


@app.get("/recipes", response_model=list[schemas.RecipeForList])
async def get_recipes():
    """
    To get the list of recipes sorted by rating(desc), if the ratings the same,
    then the sorting is done by cooking time(asc).
    """
    recipes = await session.execute(select(models.Recipe).order_by(
        desc(models.Recipe.rating)
    ).order_by(
        models.Recipe.cooking_time
    ))
    return recipes.scalars().all()


@app.get("/detailed_recipe/{r_id}", response_model=schemas.RecipeDetail)
async def get_detailed_recipe(r_id: int = Path(
    title="Id of recipe",
    description="ID of recipe in database",
    gt=0,
    le=3
)):
    """
    To get ditail info about recipe by ID.
    """
    recipe = await session.execute(select(models.Recipe).where(models.Recipe.id == r_id))
    recipe = recipe.scalar()
    for index, ingredient in enumerate(recipe.ingredients):
        ingredient.amount = recipe.ingredients_of_recipe[index].amount_of
    recipe.rating += 1
    session.add(recipe)
    await session.commit()
    return recipe
