from fastapi.testclient import TestClient
from app.main import app


client = TestClient(app)


def test_it_works():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "it works"}


def test_get_recipes():
    response = client.get("/recipes")
    template = [
        {
            "title": "Пельмени",
            "rating": 9,
            "cooking_time": 30,
            "id": 1
        },
        {
            "title": "Жареная картошка",
            "rating": 8,
            "cooking_time": 20,
            "id": 2
        },
        {
            "title": "Блины",
            "rating": 4,
            "cooking_time": 25,
            "id": 3
        },
        {
            "title": "Борщ",
            "rating": 10,
            "cooking_time": 60,
            "id": 4
        }
    ]
    assert response.status_code == 200
    for recipe in response.json():
        assert recipe["title"] in [r['title'] for r in template]


def test_get_detailed_recipe():
    response = client.get("/detailed_recipe/1")
    template = {
        "title": "Пельмени",
        "rating": 10,
        "cooking_time": 30,
        "id": 1,
        "ingredients": [
            {
                "title": "Мука",
                "id": 1,
                "amount": 500
            },
            {
                "title": "Соль",
                "id": 2,
                "amount": 3
            },
            {
                "title": "Вода",
                "id": 3,
                "amount": 200
            },
            {
                "title": "Яйцо куриное",
                "id": 4,
                "amount": 50
            },
            {
                "title": "Мясо",
                "id": 5,
                "amount": 300
            }
        ]
    }
    assert response.status_code == 200
    assert response.json()["title"] == template["title"]
    assert response.json()["cooking_time"] == template["cooking_time"]
    for ingredient in response.json()['ingredients']:
        assert ingredient["title"] in [ingr["title"] for ingr in template["ingredients"]]
    for ingredient in response.json()['ingredients']:
        assert ingredient["amount"] in [ingr["amount"] for ingr in template["ingredients"]]


def test_get_detailed_recipe_with_id_equal_zero():
    response = client.get('/detailed_recipe/0')
    assert response.status_code == 422


def test_get_detailed_recipe_with_id_equal_four():
    response = client.get('/detailed_recipe/4')
    assert response.status_code == 422
