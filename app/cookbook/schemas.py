from typing import Annotated

from pydantic import BaseModel, Field


class BaseIngredient(BaseModel):
    title: Annotated[str, Field(..., description="Title of the ingredient")]


class IngredientIn(BaseIngredient):
    ...


class IngredientOut(BaseIngredient):
    id: Annotated[
        int, Field(..., description="ID of the ingredient in the DB")
    ]
    amount: Annotated[
        int,
        Field(
            ..., description="Amount of the ingredient in the definite recipe"
        ),
    ]

    class Config:
        from_attributes = True


class BaseRecipe(BaseModel):
    title: Annotated[str, Field(..., description="Title of the recipe")]
    rating: Annotated[
        int,
        Field(
            ...,
            description="Rating of the recipe in the service. "
            "Equal to the number of views",
        ),
    ]
    cooking_time: Annotated[
        int,
        Field(
            ...,
            description="The time in seconds that you need to "
            "spend on cooking a dish",
        ),
    ]


class RecipeIn(BaseRecipe):
    ...


class RecipeForList(BaseRecipe):
    id: Annotated[int, Field(..., description="ID of recipe in DB")]

    class Config:
        from_attributes = True


class RecipeDetail(BaseRecipe):
    id: Annotated[int, Field(..., description="ID of recipe in DB")]
    ingredients: Annotated[
        list[IngredientOut],
        Field(
            ...,
            description="A list of ingredients that you need to "
            "purchase to prepare this dish",
        ),
    ]

    class Config:
        from_attributes = True
