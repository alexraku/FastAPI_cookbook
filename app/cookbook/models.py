from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.cookbook.database import Base


class Recipe(Base):
    """
    Объект для таблицы рецептов. Многие ко многим к ингредиентам через
    вспомогательную таблицу IngredientsOfRecipe
    """

    __tablename__ = "recipes"
    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str]
    cooking_time: Mapped[int]
    rating: Mapped[int] = mapped_column(default=0)

    ingredients: Mapped[list["Ingredient"]] = relationship(
        secondary="ingredients_of_recipes",
        back_populates="recipes",
        lazy="selectin",
        viewonly=True,
    )
    ingredients_of_recipe: Mapped[list["IngredientsOfRecipe"]] = relationship(
        back_populates="recipe", lazy="selectin", cascade="all, delete-orphan"
    )


class Ingredient(Base):
    """
    Объект для таблицы ингредиентов. Многие ко многим к рецептам через
    вспомогательную таблицу IngredientsOfRecipe
    """

    __tablename__ = "ingredients"
    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str]

    recipes: Mapped[list["Recipe"]] = relationship(
        secondary="ingredients_of_recipes",
        back_populates="ingredients",
        viewonly=True,
    )
    recipes_of_ingredient: Mapped[list["IngredientsOfRecipe"]] = relationship(
        back_populates="ingredient", cascade="all, delete-orphan"
    )


class IngredientsOfRecipe(Base):
    """
    Вспомогательная таблица для хранения связей рецептов с ингредиентами.
    Так же в этой таблице хранится количество ингредиента в рецепте.
    """

    __tablename__ = "ingredients_of_recipes"
    recipe_id: Mapped[int] = mapped_column(
        ForeignKey("recipes.id"), primary_key=True
    )
    ingredient_id: Mapped[int] = mapped_column(
        ForeignKey("ingredients.id"), primary_key=True
    )
    amount_of: Mapped[int]

    recipe: Mapped["Recipe"] = relationship(
        back_populates="ingredients_of_recipe"
    )
    ingredient: Mapped["Ingredient"] = relationship(
        back_populates="recipes_of_ingredient"
    )
