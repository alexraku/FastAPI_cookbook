import os

from fabric import Connection, task


@task
def deploy(ctx):
    with Connection(
        os.environ["EC2_HOST"],
        user=os.environ["EC2_USER_NAME"],
        connect_kwargs={"key_filename": os.environ["EC2_PRIVATE_KEY"]}
    ) as c:
        with c.cd("/root/FastAPI_cookbook"):
            c.run("docker compose down")
            c.run("git pull origin main --rebase")
            c.run("docker compose up --build -d")
