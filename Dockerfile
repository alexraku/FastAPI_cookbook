FROM python:3.12

COPY requirements.txt ./requirements.txt

ENV PYTHONPATH=./app
RUN pip install -r requirements.txt
COPY ./app ./app

CMD ["python", "-m", "gunicorn", "app.main:app", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:8080"]